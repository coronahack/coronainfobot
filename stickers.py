import json
from telegram.ext import MessageHandler, ConversationHandler, Filters
import sys


class StickersWrapper:

    STICKER_NAME = 0

    def __init__(self):
        with open('stickers.json') as fp:
            self.d = json.load(fp)
        self.tmp_file_id = None
        self._handler = None

    def save(self):
        with open('stickers.json', 'w') as fp:
            json.dump(self.d, fp, indent=2, sort_keys=True)

    def ask_sticker_name(self, update, context):
        self.tmp_file_id = update.message.sticker.file_id
        update.message.reply_text("Пришлите название стикера")
        return self.STICKER_NAME

    def save_sticker_name(self, update, context):
        self.d[update.message.text] = self.tmp_file_id
        self.save()
        update.message.reply_text("Стикер сохранен, спасибо!")
        return ConversationHandler.END
    
    @property
    def handler(self):
        if self._handler is None:
            self._handler = ConversationHandler(
                entry_points=[MessageHandler(Filters.sticker, self.ask_sticker_name)],

                states={self.STICKER_NAME: [MessageHandler(Filters.text, self.save_sticker_name)]},

                fallbacks=[]
            )
        return self._handler

    def __getattr__(self, item):
        return self.d[item]


sys.modules[__name__] = StickersWrapper()
