import sys
import markup


class MessageGenerator:
    def __init__(self, user=None):
        self.user = user
        self.markup = markup.for_(user)

    @staticmethod
    def for_(user):
        return MessageGenerator(user)

    @property
    def whappens(self):
        if self.user.have_hospitalized and not self.user.have_symptoms:
            pre_text = "Итак, вас госпитализировали. Вы можете подробнее *рассказать о своих симптомах*. "\
                       "Если с вами будет что-то происходить во время госпитализации, "\
                       "вы можете *обновить статус госпитализации*."
        elif self.user.have_hospitalized and self.user.have_symptoms:
            pre_text = "Итак, вас госпитализировали и вы рассказали о ваших симптомах. "\
                       "Если с вами будет что-то происходить во время госпитализации, "\
                       "вы можете *обновить статус госпитализации*. Если ваши симптомы изменятся, "\
                       "вы всегда можете их *обновить*."
        elif not self.user.have_hospitalized and self.user.have_symptoms:
            pre_text = "Вы рассказали о ваших симптомах. " \
                       "Если вас повезут в больницу или вы уже там, нажмите *Меня госпитализируют*. " \
                       "Если ваши симптомы изменятся, вы всегда можете их *обновить* и получить новую рекомендацию."
        elif not self.user.have_hospitalized and not self.user.have_symptoms:
            pre_text = "Что с {0} произошло?".format('вами' if self.user.telling_about_myself else 'вашим близким')
                       # "Если вы испытываете подозрительные *симптомы*, расскажите о них и вы получите рекомендацию. "
                       # "Если вас везут в больницу или вы уже там, нажмите *Меня госпитализируют*."
        suf_text = "\nЧтобы подробнее узнать о моих функциях, нажмите /help."
        return {"text": "{0} {1}".format(pre_text, suf_text),
                "parse_mode": "Markdown",
                "reply_markup": self.markup.whappens
                }


sys.modules[__name__] = MessageGenerator