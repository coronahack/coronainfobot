from telegram import ParseMode
import res
import markup
import user


def get(*path):
    return res.txts[res.path_to_txt(*path)]


def callback(*path, state=None, parse_mode=ParseMode.MARKDOWN_V2, sticker=None):
    t = get(*path)

    @user.wrap
    def cb(usr, update, context):
        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text=t,
                                 parse_mode=parse_mode,
                                 reply_markup=markup.ok,
                                 disable_web_page_preview=True)
        if sticker is not None:
            context.bot.send_sticker(chat_id=update.effective_chat.id,
                                     sticker=sticker)
        usr.trace(" ".join(path), "text")
        return state
    return cb
