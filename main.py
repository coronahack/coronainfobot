import bot
import tree.root
import config
if config.developer_mode: import dev_features

bot.dp.add_handler(tree.root.handler)

def main():
    """запускаем бота настраиваем pycharm"""
    bot.upd.start_polling()
    bot.upd.idle()


if __name__ == '__main__':
    main()
