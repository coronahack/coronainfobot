from telegram.ext import ConversationHandler
import user
import markup

class MainTreeState: pass

MAIN = MainTreeState()
END = ConversationHandler.END


@user.wrap
def return_to_main(usr, upd, cnt):
    usr.cancel()
    upd.message.reply_text(text="Чем еще я могу быть полезен?\n"
                                "Если вы потерялись, нажмите /help.",
                           reply_markup=markup.main)
    return MAIN


def wrong(state):
    def cb(upd, cnt):
        upd.message.reply_text("Некорректный ввод, попробуйте снова",
                               reply_markup=markup.undo)
        return state
    return cb
