symptoms = ["Сухой кашель", "Одышка", "Температура > 38", "Головная боль", "Боль в груди",
            "Усталость", "Насморк", "Боль в костях и мышцах",
            "Боль в горле",  "Потеря обоняния или вкуса", "Диарея"]
corona = ["Сухой кашель", "Одышка", "Температура > 38", "Боль в груди"]
light_symptoms = set(symptoms).difference(set(corona))
path = './res/ответы на симптомы/'
temperature = "Температура > 38"


def symptoms_decision(user_symptoms, group_risk, potential_contacts):
    set1 = set(user_symptoms)
    all_intersection = set1.intersection(set(symptoms))
    heavy_intersection = set1.intersection(set(corona))
    light_intersection = set1.intersection(light_symptoms)

    if len(heavy_intersection) == 0:
        if group_risk:
            if len(all_intersection) >= 3:
                #print(getframeinfo(currentframe()).lineno)
                return open('{}все не так страшно.txt'.format(path)).read()
            #print(getframeinfo(currentframe()).lineno)
            return open('{}опасения невилики.txt'.format(path)).read()

        if potential_contacts:
            if len(all_intersection) >= 3:
                #print(getframeinfo(currentframe()).lineno)
                return open('{}все не так страшно.txt'.format(path)).read()
            #print(getframeinfo(currentframe()).lineno)
            return open('{}опасения невилики.txt'.format(path)).read()

        if len(all_intersection) == 3:
            #print(getframeinfo(currentframe()).lineno)
            return open('{}все не так страшно.txt'.format(path)).read()

        if len(all_intersection) > 3:
            #print(getframeinfo(currentframe()).lineno)
            return open('{}опасные_симптомы.txt'.format(path)).read()

        #print(getframeinfo(currentframe()).lineno)
        return open('{}легкие_нет_нет.txt'.format(path)).read()

    if len(heavy_intersection) == 1:
        if group_risk:
            if len(all_intersection) >= 3 or temperature in heavy_intersection:
                #print(getframeinfo(currentframe()).lineno)
                return open('{}группа риска.txt'.format(path)).read()
            else:
                return open('{}опасения невилики.txt'.format(path)).read()

        if potential_contacts:
            if len(all_intersection) >= 3 or temperature in heavy_intersection:
                #print(getframeinfo(currentframe()).lineno)
                return open('{}потенциальные контакты.txt'.format(path)).read()
            else:
                return open('{}опасения невилики.txt'.format(path)).read()

        if temperature in heavy_intersection and len(all_intersection) <= 3:
            #print(getframeinfo(currentframe()).lineno)
            return open('{}все не так страшно.txt'.format(path)).read()

        if len(all_intersection) > 3:
            #print(getframeinfo(currentframe()).lineno)
            return open('{}опасные_симптомы.txt'.format(path)).read()

        #print(getframeinfo(currentframe()).lineno)
        return open('{}один основной симптом или два легких.txt'.format(path)).read()

    if 0 < len(heavy_intersection) <= 2:
        if len(heavy_intersection) == 2 and temperature in heavy_intersection:
            if group_risk:
                #print(getframeinfo(currentframe()).lineno)
                return open('{}два и и более основных, температура 38.5_нет_да.txt'.format(path)).read()

            if potential_contacts:
                #print(getframeinfo(currentframe()).lineno)
                return open('{}два и и более основных, температура 38.5_да_нет.txt'.format(path)).read()

            #print(getframeinfo(currentframe()).lineno)
            return open('{}два и и более основных, температура 38.5_нет_нет.txt'.format(path)).read()

        if group_risk:
            #print(getframeinfo(currentframe()).lineno)
            return open('{}два и более основных + легкие_нет_да.txt'.format(path)).read()

        if potential_contacts:
            #print(getframeinfo(currentframe()).lineno)
            return open('{}два и более основных + легкие_да_нет.txt'.format(path)).read()

        #print(getframeinfo(currentframe()).lineno)
        return open('{}опасные_симптомы.txt'.format(path)).read()

    if len(heavy_intersection) >= 2 and temperature in heavy_intersection:
        if group_risk:
            #print(getframeinfo(currentframe()).lineno)
            return open('{}два и и более основных, температура 38.5_нет_да.txt'.format(path)).read()

        if potential_contacts:
            #print(getframeinfo(currentframe()).lineno)
            return open('{}два и и более основных, температура 38.5_да_нет.txt'.format(path)).read()

        #print(getframeinfo(currentframe()).lineno)
        return open('{}два и и более основных, температура 38.5_нет_нет.txt'.format(path)).read()