# -*- coding: utf-8 -*-

import logging
import traceback
import json

from telegram import Bot
from telegram.utils.request import Request
from telegram.ext import Updater

import config

if config.use_proxy:
    req = Request(proxy_url=config.proxy,
                  urllib3_proxy_kwargs={'username':config.proxy_username,
                                        'password':config.proxy_password})
else:
    req = Request()

bot = Bot(config.token, request=req)
upd = Updater(bot=bot, use_context=True)
dp = upd.dispatcher

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)


def error(update, context):
    logger = logging.getLogger()
    update_text = json.dumps(update.to_dict() if update else None, indent=2, ensure_ascii=False)
    warn_update = f'Update: "{update_text}"'
    warn_error = f'Error: {context.error.__class__.__name__} "{context.error}"\n\n' \
            f'Traceback:\n{str().join(traceback.format_tb(context.error.__traceback__))}'
    logger.warning(warn_update)
    logger.warning(warn_error)
    bot.send_message(chat_id=config.errorlog,
                     text=warn_update)
    bot.send_message(chat_id=config.errorlog,
                     text=warn_error)


dp.add_error_handler(error)

