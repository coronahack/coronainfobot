import json
import sys
import os


class ConfigWrapper:
    def __init__(self, d=dict()):
        self.main = d
        self.env = d.get('env') or dict()
        self.type = d.get('type') or dict()

    @staticmethod
    def _apply_type(value, type_str):
        if type(value) != str: return value
        if type_str == 'bool':
            if value.lower() == 'true' or value.lower() == '1':
                return True
            else:
                return False
        elif type_str == 'int':
            return int(value)
        else:
            return value

    def __getattr__(self, item):
        env_v = self.env.get(item)
        main_v = self.main.get(item)
        type_v = self.type.get(item)

        if env_v:
            v = os.environ.get(self.env.get(item))
            if not v: OSError('Environment variable "{0}" is empty'.format(env_v))
            return self._apply_type(v, type_v)
        else:
            v = main_v

        if type_v:
            return self._apply_type(v, type_v)
        else:
            return v



with open('bot_config.json') as cfg:
    sys.modules[__name__] = ConfigWrapper(json.load(cfg))
