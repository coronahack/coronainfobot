import os
import os.path

RES_DIR = 'res/'


def txt_filter(txt):
    return txt.replace(u'\xa0', u' ')


def load_txt_recursive(fp):
    d = dict()
    for fn in os.listdir(fp):
        if fn.endswith('.txt'):
            k = os.path.join(fp, fn[:-4]).strip().lower()
            d[k] = txt_filter(open(os.path.join(fp, fn)).read())
        elif os.path.isdir(os.path.join(fp, fn)):
            d.update(load_txt_recursive(os.path.join(fp, fn)))
    return d


def path_to_txt(*path):
    return os.path.join(RES_DIR, *path).strip().lower()


txts = load_txt_recursive(RES_DIR)
