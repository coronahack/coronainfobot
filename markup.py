from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove, 
                        KeyboardButton)
import sys


class MarkupGenerator:
    def __init__(self, user=None):
        self.user = user

    @staticmethod
    def for_(user):
        return MarkupGenerator(user)

    main = ReplyKeyboardMarkup([[KeyboardButton(text='Нужна помощь'),
                                 KeyboardButton(text='Хочу помочь')],
                                [KeyboardButton(text='Полезно знать')]])

    @property
    def whappens(self):
        if self.user.have_symptoms:
            symptoms_text = 'Обновить симптомы'
        elif self.user.have_hospitalized:
            symptoms_text = 'Рассказать о симптомах'
        else:
            symptoms_text = 'Обнаружил(а) симптомы'

        if self.user.have_hospitalized:
            hospital_text = 'Обновить статус госпитализации'
        else:
            hospital_text = 'Меня госпитализируют'

        hospital_buttons = [KeyboardButton(text=hospital_text)]
        if self.user.have_hospitalized:
            hospital_buttons.append(KeyboardButton(text="Не устраивает медицинская помощь"))

        return ReplyKeyboardMarkup(
            [[KeyboardButton(text=symptoms_text),
              KeyboardButton(text='Мои права нарушают')],
             hospital_buttons,
             [KeyboardButton(text='Другая помощь'), KeyboardButton(text='Вернуться')]],
            one_time_keyboard=True,
            resize_keyboard=True)

    other_help = ReplyKeyboardMarkup([[KeyboardButton(text="Связаться с волонтерами")],
                                      [KeyboardButton(text="Психологическая помощь")],
                                      [KeyboardButton(text="Помощь врачам"),
                                       KeyboardButton(text="Продукты питания")],
                                      [KeyboardButton(text="Вернуться")]])

    types_of_offense = ReplyKeyboardMarkup([[KeyboardButton(text="Задержали полицейские")],
                                            [KeyboardButton(text="Принудительно госпитализируют")],
                                            [KeyboardButton(text="Проблемы в университете"),
                                             KeyboardButton(text="Проблемы с работой")],
                                            [KeyboardButton(text="Вернуться")]])

    types_of_guidance_offense = ReplyKeyboardMarkup([[KeyboardButton(text="Что будет, если нарушить карантин?")],
                                                     [KeyboardButton(text="Как вести себя при задержании?")],
                                                     [KeyboardButton(text="Могут ли меня уволить?"),
                                                      KeyboardButton(text="Вернуться")]])

    work_troubles = ReplyKeyboardMarkup([[KeyboardButton(text="Сократили на работе")],
                                         [KeyboardButton(text="Работодатель подвергает риску")],
                                         [KeyboardButton(text="Вернуться")]])

    fake_symptoms = ReplyKeyboardMarkup([[KeyboardButton(text='Ввести симптомы')]])

    diagnosis = ReplyKeyboardMarkup(
        [[KeyboardButton(text='Еще нет'), KeyboardButton(text='Коронавирус')],
         [KeyboardButton(text='Внебольничная пневмония')],
         [KeyboardButton(text='Другой диагноз'), KeyboardButton(text='Не хочу сообщать')]],
        one_time_keyboard=True)

    is_hospital = ReplyKeyboardMarkup(
                        [
                        [KeyboardButton(text='Дома'), KeyboardButton(text='В больнице')],
                        [KeyboardButton(text='Отмена')]
                        ],
                        one_time_keyboard=True
                    )

    yesno = ReplyKeyboardMarkup(
                    [[KeyboardButton(text='Да'), KeyboardButton(text='Нет')]],
                    one_time_keyboard=True,
                    resize_keyboard=True
                    )

    yesno_back = ReplyKeyboardMarkup(
                    [[KeyboardButton(text='Да'), KeyboardButton(text='Нет'),
                      KeyboardButton(text='Вернуться')]],
                    one_time_keyboard=True,
                    resize_keyboard=True
                    )

    diagn_hospital = ReplyKeyboardMarkup(
                    [
                        KeyboardButton(text='Ожидаю'),
                        KeyboardButton(text='Коронавирус'),
                        KeyboardButton(text='Внебольничная пневмония'),
                        KeyboardButton(text='Другой диагноз')
                    ],
                    one_time_keyboard=True,
                    )

    contact = ReplyKeyboardMarkup(
        [
            [KeyboardButton(text='Поделиться контактом', request_contact=True)],
            [KeyboardButton(text='Не хочу')]
        ]
    )

    @property
    def location(self):
        return ReplyKeyboardMarkup(
            [[KeyboardButton(text='Прислать текущее местоположение' if self.user.telling_about_myself else
                                  'Рядом со мной (прислать текущее местоположение)',
                             request_location=True)],
            [KeyboardButton(text='Указать на карте'), KeyboardButton(text='Прислать индекс')],
            [KeyboardButton(text='Вернуться')]])

    location_short = ReplyKeyboardMarkup(
        [[KeyboardButton(text='Прислать геометку', request_location=True),
          KeyboardButton(text='Отмена')]],
        resize_keyboard=True,
        one_time_keyboard=True)

    postal_code = ReplyKeyboardMarkup(
        [[KeyboardButton(text='Не в РФ'), KeyboardButton(text='Отмена')]],
        resize_keyboard=True)

    end = ReplyKeyboardMarkup([[KeyboardButton(text='Вернуться')]],
                              one_time_keyboard=True,
                              resize_keyboard=True)

    undo = ReplyKeyboardMarkup([[KeyboardButton(text='Отмена')]],
                               one_time_keyboard=True,
                               resize_keyboard=True)

    ok = ReplyKeyboardMarkup([[KeyboardButton(text='Понятно')]],
                             one_time_keyboard=True,
                             resize_keyboard=True)

    other_choice = ReplyKeyboardMarkup(
                    [[KeyboardButton(text='О новой персоне')],
                     [KeyboardButton(text='О которой я уже рассказывал')],
                     [KeyboardButton(text='Вернуться')]],
                    one_time_keyboard=True)

    other_choice_full = ReplyKeyboardMarkup(
                    [[KeyboardButton(text='О которой я уже рассказывал')],
                     [KeyboardButton(text='Вернуться')]],
                    one_time_keyboard=True)

    data_agree = ReplyKeyboardMarkup(
                    [[KeyboardButton(text='Да, разрешаю')],
                     [KeyboardButton(text='Нет, не разрешаю')],
                     [KeyboardButton(text='Я просто тестирую бота')]],
                    one_time_keyboard=True)

    continue_or_not = ReplyKeyboardMarkup(
        [[KeyboardButton(text='Продолжить'), KeyboardButton(text='Вернуться')]],
        resize_keyboard=True,
        one_time_keyboard=True)

    contrib = ReplyKeyboardMarkup([[KeyboardButton(text="Поддержать НКО и местные инициативы")],
                                   [KeyboardButton(text="Распространить полезную информацию"),
                                    KeyboardButton(text="Стать волонтером")],
                                  [KeyboardButton(text="Обратная связь"), KeyboardButton(text="Вернуться")]])

    contrib_local = ReplyKeyboardMarkup([[KeyboardButton(text="Уязвимым людям")],
                                         [KeyboardButton(text="Медицинским работникам")],
                                         [KeyboardButton(text="Малому бизнесу и культуре")],
                                         [KeyboardButton(text="Вернуться")]])

    contrib_volunteer = ReplyKeyboardMarkup([[KeyboardButton(text="У меня есть медицинские навыки")],
                                             [KeyboardButton(text="Хочу помочь нуждающимся")],
                                             [KeyboardButton(text="Вернуться")]])

    types_of_help = ReplyKeyboardMarkup([[KeyboardButton(text="Напомнить меры профилактики"),
                                          KeyboardButton(text="Советы юристов")],
                                         [KeyboardButton(text="Вернуться")]])

    rights_medical = ReplyKeyboardMarkup([[KeyboardButton(text="Не говорят результаты тестов")],
                                          [KeyboardButton(text="Поместили в общую палату с подозрением на коронавирус")],
                                          [KeyboardButton(text="Вернуться")]])

    rights_hospital = ReplyKeyboardMarkup([[KeyboardButton(text="Я против госпитализации. Что делать?")],
                                           [KeyboardButton(text="Мне угрожают уголовной ответственностью. Это законно?")],
                                           [KeyboardButton(text="Вернуться")]])

    faq_questions = ReplyKeyboardMarkup([
        [KeyboardButton(text='Как не заразиться?'),
         KeyboardButton(text='Как не заразить окружающих?')],
        [KeyboardButton(text='Когда стоит вызывать врача?'),
         KeyboardButton(text='Вернуться')]])

    only_return = ReplyKeyboardMarkup([
        [KeyboardButton(text='Вернуться')]
    ])

    remove = ReplyKeyboardRemove()



sys.modules[__name__] = MarkupGenerator
