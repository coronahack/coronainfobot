from telegram.ext import ConversationHandler, CommandHandler, MessageHandler, Filters
from telegram import ParseMode

import user
import markup
import scenario
import stickers
import text

import tree.needhelp
import tree.guidance
import tree.contribute


@user.wrap
def hello(u, update, context):
    u.start()
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="""Привет! Это бот «Новой газеты» и команды хактивистов CoronaHack. 

Мы собрали в одном месте полезные инструменты, которые помогают бороться с пандемией COVID-2019 в России.

Здесь вы сможете: 

 - узнать, как вести себя в сложной ситуации; 
 - обратиться за помощью к волонтерам;  
 - поддержать пострадавших от пандемии;
 - стать добровольцем в общественном проекте. 

Для более подробной информации нажмите /help.

С чего начнем?""",
                             reply_markup=markup.main)
    context.bot.send_sticker(chat_id=update.effective_chat.id,
                             sticker=stickers.crown)
    return scenario.MAIN


@user.wrap
def remind_help(usr, upd, cnt):
    upd.message.reply_text(text.get('help'),
                           parse_mode=ParseMode.MARKDOWN_V2)
    return scenario.MAIN


handler = ConversationHandler(
    entry_points=[MessageHandler(Filters.all, hello)],

    states={
        scenario.MAIN: [CommandHandler('help', remind_help),
                        tree.needhelp.handler,    # нужна помощь
                        tree.guidance.handler,    # полезно знать
                        tree.contribute.handler,  # хочу помочь
                        CommandHandler('start', scenario.return_to_main)
                        ]
             },

    fallbacks=[] # возможно стоит обрабатываnь отдельно если люди что-то пишут в бота, предлагать им помощь
)
