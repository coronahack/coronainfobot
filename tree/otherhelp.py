from telegram import ParseMode
from telegram.ext import ConversationHandler, MessageHandler, Filters, CommandHandler, CallbackQueryHandler

import user
import scenario
import markup
import text
import api


OTHER_HELP, ASK_LOCATION = range(30, 32)


@user.wrap
def other_help(usr, upd, cnt):
    upd.message.reply_text("Как еще вам можно помочь?",
                           reply_markup=markup.other_help)
    return OTHER_HELP


@user.wrap
def covidarnost_info(usr, upd, cnt):
    upd.message.reply_text(text.get('нужна помощь', 'другая помощь', 'Связать с ближайшей группой поддержки'),
                           parse_mode="MarkdownV2")
    return connect_local(upd, cnt)


@user.wrap
def connect_local(u, update, context):
    u.focus_on_myself()
    if u.have_geo_position:
        local_chat = api.request_local_chat(u.geo_position)
        if local_chat:
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text='Мы нашли местный чат!' +
                                          'Этот чат создан и поддерживается дружественным проектом ' +
                                          '[COVIDарность](https://t.me/covidarnost)',
                                     parse_mode=ParseMode.MARKDOWN)
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text=local_chat['url'],
                                     reply_markup=markup.ok)
            return OTHER_HELP
        else:
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text='К сожалению, мы не смогли найти чат для вашего района. ' +
                                          'Вы можете обратиться к боту дружественного проекта COVIDарность ' +
                                          '@covid_tgbot чтобы создать чат поддержки вашего дома.',
                                     reply_markup=markup.ok)
            return OTHER_HELP

    else:
        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text="Пришлите вашу геолокацию, чтобы мы нашли чат волонтеров поблизости.",
                                 reply_markup=markup.location_short)
        return ASK_LOCATION


@user.wrap
def proceed_location(u, update, context):
    u.save_geo_position(update.message.location)
    return connect_local(update, context)


handler = ConversationHandler(
    entry_points=[
        MessageHandler(Filters.regex('^(Другая помощь)$'), other_help)
    ],

    states={

        OTHER_HELP: [MessageHandler(Filters.regex('^(Понятно)$'), other_help),
                     MessageHandler(Filters.regex('^(Связаться с волонтерами)$'), covidarnost_info),
                     MessageHandler(Filters.regex('^(Продукты питания)$'),
                                    text.callback('нужна помощь', 'другая помощь', 'Продукты питания',
                                                  state=OTHER_HELP)),
                     MessageHandler(Filters.regex('^(Психологическая помощь)$'),
                                    text.callback('нужна помощь', 'другая помощь', 'Психологическая помощь',
                                                  state=OTHER_HELP)),
                     MessageHandler(Filters.regex('^(Помощь врачам)$'), text.callback('нужна помощь', 'другая помощь',
                                                                                      'Я врач, и мне нужна помощь',
                                                                                      state=OTHER_HELP))
                     ],

        ASK_LOCATION: [MessageHandler(Filters.location, proceed_location),
                       MessageHandler(Filters.all, other_help)]

    },

    fallbacks=[MessageHandler(Filters.regex('^(Вернуться)$'), scenario.return_to_main),
               CommandHandler('start', scenario.return_to_main)],

    map_to_parent={scenario.MAIN: scenario.MAIN,
                   scenario.END: scenario.MAIN}
)