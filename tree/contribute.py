from telegram.ext import CommandHandler, MessageHandler, Filters, ConversationHandler
from telegram import ReplyKeyboardMarkup, KeyboardButton, MessageEntity, ParseMode

import user
import config
import scenario
import markup
import text

MAIN, LOCAL, VOLUNTEER, FEEDBACK = range(4)

@user.wrap
def how_to_contribute(usr, upd, cnt):
    upd.message.reply_text("Отлично! Вот несколько вариантов.",
                           reply_markup=markup.contrib)
    return MAIN

@user.wrap
def finance(usr, upd, cnt):
    upd.message.reply_text("💸")
    return MAIN

@user.wrap
def local(usr, upd, cnt):
    upd.message.reply_text("Кому вы хотели бы помочь?",
                           reply_markup=markup.contrib_local)
    return LOCAL

@user.wrap
def volunteer(usr, upd, cnt):
    upd.message.reply_text("Какая форма волонтерства вам подходит?",
                           reply_markup=markup.contrib_volunteer)
    return VOLUNTEER

@user.wrap
def feedback_start(usr, upd, cnt):
    upd.message.reply_text("Напишите здесь, что вы хотели бы мне передать.",
                           reply_markup=markup.end)
    return FEEDBACK

@user.wrap
def feedback_forward(usr, upd, cnt):
    upd.message.forward(chat_id=config.feedback)
    upd.message.reply_text("Спасибо за ваше сообщение, оно передано разработчикам! " +
                           "Вы можете дополнить свое сообщение")
    return FEEDBACK


handler = ConversationHandler(
    entry_points=[MessageHandler(Filters.regex("^(Хочу помочь)$"), how_to_contribute)],

    states={
        MAIN: [MessageHandler(Filters.regex("^(Понятно)$"), how_to_contribute),
               MessageHandler(Filters.regex("^(Поддержать НКО и местные инициативы)$"), local),
               MessageHandler(Filters.regex("^(Стать волонтером)$"), volunteer),
               MessageHandler(Filters.regex("^(Обратная связь)$"), feedback_start),
               MessageHandler(Filters.regex("^(Распространить полезную информацию)$"),
                              text.callback('хочу помочь', 'Распространить полезную информацию',
                                            state=MAIN))],

        LOCAL: [MessageHandler(Filters.regex("^(Понятно)$"), local),
                MessageHandler(Filters.regex("^(Вернуться)$"), how_to_contribute),
                MessageHandler(Filters.regex("^(Медицинским работникам)$"),
                               text.callback('хочу помочь', 'Поддержать НКО и местные инициативы _Поддержать медицинских работников',
                                             state=LOCAL)),
                MessageHandler(Filters.regex("^(Малому бизнесу и культуре)$"),
                               text.callback('хочу помочь', 'Поддержать НКО и местные инициативы_Помочь малому бизнесу и культуре',
                                             state=LOCAL)),
                MessageHandler(Filters.regex("^(Уязвимым людям)$"),
                               text.callback('хочу помочь', 'Поддержать НКО и местные инициативы_Помочь уязвимым группам',
                                             state=LOCAL))
                ],

        VOLUNTEER: [MessageHandler(Filters.regex("^(Понятно)$"), volunteer),
                    MessageHandler(Filters.regex("^(Вернуться)$"), how_to_contribute),
                    MessageHandler(Filters.regex("^(У меня есть медицинские навыки)$"),
                                   text.callback('хочу помочь', 'Стать волонтером_У меня есть медицинские навыки',
                                                 state=VOLUNTEER)),
                    MessageHandler(Filters.regex("^(Хочу помочь нуждающимся)$"),
                                   text.callback('хочу помочь', 'Стать волонтером_Помочь нуждающимся',
                                                 state=VOLUNTEER)),
                    ],

        FEEDBACK: [MessageHandler(Filters.regex("^(Вернуться)$"), how_to_contribute),
                   MessageHandler(Filters.text, feedback_forward)]},

    fallbacks=[MessageHandler(Filters.regex("^(Вернуться)$"), scenario.return_to_main),
               CommandHandler('start', scenario.return_to_main)],

    map_to_parent={scenario.MAIN: scenario.MAIN}
)
