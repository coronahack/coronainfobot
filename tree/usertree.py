from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram.ext import ConversationHandler, MessageHandler, Filters, CommandHandler, CallbackQueryHandler

import user
import scenario
import story
import markup

OTHR_IS_NEW, OTHR_CHOICE, OTHR_CONTINUE,\
LOC, LOC_NOT_IN_RU, NAME, AGE = range(7)


@user.wrap
def about_myself(usr, upd, cnt):
    usr.focus_on_myself()
    return ask_age(upd, cnt)


@user.wrap
def about_other(usr, upd, cnt):
    usr.new_other()
    usr.focus_on_new_other()
    return ask_name(upd, cnt)


@user.wrap
def about_other_choice(usr, upd, cnt):
    upd.message.reply_text("Вы хотите рассказать о новой персоне, о которой мы еще не знаем, или" +
                           "о которой вы уже рассказывали?",
                           reply_markup=markup.other_choice if usr.is_available_other else markup.other_choice_full)
    return OTHR_IS_NEW


@user.wrap
def ask_name(usr, upd, cnt):
    upd.message.reply_text("Как зовут того, о ком вы рассказываете?",
                           reply_markup=markup.undo)
    return NAME


@user.wrap
def handle_name(usr, upd, cnt):
    usr.save_name(upd.message.text)
    return ask_age(upd, cnt)


@user.wrap
def ask_age(usr, upd, cnt):
    upd.message.reply_text("Сколько вам лет?" if usr.telling_about_myself else "Сколько лет этому человеку?",
                           reply_markup=markup.undo)
    return AGE


@user.wrap
def handle_age(usr, upd, cnt):
    usr.save_age(upd.message.text)
    return ask_location(upd, cnt)


@user.wrap
def ask_location(usr, upd, cnt):
    upd.message.reply_text("Где вы находитесь? " if usr.telling_about_myself else
                           "Где находится человек о котором вы говорите? " +
                           "Эти данные нужны нам для сбора статистики в обезличенном виде. " +
                           "Пришлите геолокацию или почтовый индекс.",
                           reply_markup=markup.location(usr.telling_about_myself))
    return LOC


@user.wrap
def postal_code_highlight(usr, upd, cnt):
    upd.message.reply_text('Пришлите почтовый индекс -- это 6 цифр. Узнать индекс можно на сайте ' +
                           '[почты россии](https://www.pochta.ru/post-index).',
                           parse_mode=ParseMode.MARKDOWN,
                           reply_markup=markup.postal_code)
    return LOC


@user.wrap
def not_in_russia(usr, upd, cnt):
    upd.message.reply_text('Пришлите местоположение в формате «Страна, Город»',
                           reply_markup=markup.undo)
    return LOC_NOT_IN_RU


@user.wrap
def map_location_highlight(usr, upd, cnt):
    upd.message.reply_text('Нажмите на значок скрепочки, выберите там Location, ' +
                           'укажите нужное место и отправьте его',
                           reply_markup=markup.undo)
    return LOC


@user.wrap
def get_location(usr, upd, cnt):
    usr.save_location(upd.message.location)
    return story.whappens(upd, cnt)


@user.wrap
def get_postal_code(usr, upd, cnt):
    usr.save_postal_code(upd.message.text)
    return story.whappens(upd, cnt)


@user.wrap
def get_text_location(usr, upd, cnt):
    usr.save_text_location(upd.message.text)
    return story.whappens(upd, cnt)


@user.wrap
def update_other(usr, upd, cnt):
    kb = [[InlineKeyboardButton(n, callback_data=str(i))] for i, n in enumerate(usr.names_of_others)]
    upd.message.reply_text("О ком вы хотите уточнить информацию?",
                           reply_markup=InlineKeyboardMarkup(kb))
    return OTHR_CHOICE


@user.wrap
def update_other_button(usr, upd, cnt):
    query = upd.callback_query
    query.answer()
    usr.focus_on(int(query.data))
    query.edit_message_text("Вы выбрали: {0}️".format(usr.name),
                            reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton('Продолжить', 'continue')]]))
    return OTHR_CONTINUE


handler = ConversationHandler(
    entry_points=[
        MessageHandler(Filters.regex('^(Рассказать о себе)$') & user.Filter.told_about_myself,
                       story.whappens),
        MessageHandler(Filters.regex('^(Рассказать о себе)$'), about_myself),
        MessageHandler(Filters.regex('^(Рассказать о другом человеке)$') & user.Filter.told_about_other,
                       about_other_choice),
        MessageHandler(Filters.regex('^(Рассказать о другом человеке)$'), about_other)
    ],

    states={
        LOC: [MessageHandler(Filters.location, get_location),
              MessageHandler(Filters.regex('^[0-9]{6}$'), get_postal_code),
              MessageHandler(Filters.regex('^(Указать на карте)$'), map_location_highlight),
              MessageHandler(Filters.regex('^(Прислать индекс)$'), postal_code_highlight),
              MessageHandler(Filters.regex('^(Не в России)$'), not_in_russia),
              MessageHandler(Filters.regex('^(Отмена)$'), ask_location),
              MessageHandler(~Filters.regex('^(Вернуться|/cancel)$'), ask_location)],

        LOC_NOT_IN_RU: [
            MessageHandler(Filters.regex(r'^\D, \D$'), get_text_location),
            MessageHandler(Filters.regex('^(Отмена)$'), ask_location),
            MessageHandler(Filters.all, not_in_russia)],

        NAME: [MessageHandler(Filters.regex('^(Отмена|/cancel)$'), scenario.return_to_main),
               MessageHandler(Filters.text, handle_name),
               MessageHandler(Filters.all, scenario.wrong(NAME))],

        AGE: [MessageHandler(Filters.regex('^(Отмена|/cancel)$'), scenario.return_to_main),
              MessageHandler(Filters.regex('^[0-9]{1,3}$'), handle_age),
              MessageHandler(Filters.all, scenario.wrong(AGE))],

        story.WHAT_HAPPENS: [story.handler],

        OTHR_IS_NEW: [MessageHandler(Filters.regex('^(О новой персоне)$') & user.Filter.available_other, about_other),
                      MessageHandler(Filters.regex('^(О которой я уже рассказывал)$'), update_other)],

        OTHR_CHOICE: [CallbackQueryHandler(update_other_button)],

        OTHR_CONTINUE: [CallbackQueryHandler(story.whappens)]
    },

    fallbacks=[MessageHandler(Filters.regex('^(Вернуться)$'), scenario.return_to_main),
               CommandHandler('cancel', scenario.return_to_main)],

    map_to_parent={scenario.MAIN: scenario.MAIN},

    per_chat=False
)
