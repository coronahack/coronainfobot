from telegram import InlineKeyboardButton, InlineKeyboardMarkup

import user
import symptoms_decision

import itertools

full_symptoms_list = symptoms_decision.symptoms

MAIN = 125


def grouper(n, iterable):
    args = [iter(iterable)] * n
    return [[e for e in t if e is not None] for t in itertools.zip_longest(*args)]


def alternating_merge(*iterables):
    l = list()
    for izip in itertools.zip_longest(*iterables):
        for i in izip:
            if i is not None: l.append(i)
    return l


def generate_symptoms_keyboard(symptoms):
    long_buttons = []
    small_buttons = []
    super_small_buttons = []

    for symptom in sorted(full_symptoms_list):
        if len(symptom) >= 14:
            cur = long_buttons
        elif len(symptom) <= 6:
            cur = super_small_buttons
        else:
            cur = small_buttons
        cur.append(InlineKeyboardButton("{0}{1}".format("✅ " if symptom in symptoms else "", symptom),
                                        callback_data=symptom))

    if len(super_small_buttons) >= 3:
        super_small_kb = [super_small_buttons[:3]]
        small_buttons.extend(super_small_buttons[3:])
    else:
        super_small_kb = []
        small_buttons.extend(super_small_buttons)

    long_kb = [[b] for b in long_buttons]
    small_kb = grouper(2, small_buttons)

    kb = alternating_merge(long_kb, small_kb, super_small_kb)
    kb.append([InlineKeyboardButton("❇️ Завершить ввод симптомов", callback_data="end")])

    return InlineKeyboardMarkup(kb)


@user.wrap
def callback(usr, update, context):
    usr.tmp_symptoms = []
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="Выберите свои симптомы.",
                             reply_markup=generate_symptoms_keyboard(usr.tmp_symptoms))
    return MAIN


def button(next_step):
    @user.wrap
    def button_callback(usr, update, context):
        query = update.callback_query
        #kb = [[InlineKeyboardButton(n, callback_data=str(i))] for i, n in enumerate(usr.names_of_others())]
        if query.data in usr.tmp_symptoms:
            usr.tmp_symptoms.remove(query.data)
        elif query.data == "end":
            usr.save_symptoms(usr.tmp_symptoms)
            return next_step(update, context)
        else:
            usr.tmp_symptoms.append(query.data)

        if not usr.tmp_symptoms:
            query.answer()
            query.edit_message_text(text="Вы удалили все выбранные опции. Отметьте субъективно наблюдаемые симптомы или завершите ввод симптомов: ",
                                    reply_markup=generate_symptoms_keyboard(usr.tmp_symptoms))
        else:
            query.answer()
            query.edit_message_text(text="Отмеченные симптомы: \n✔️" + "\n✔️".join(usr.tmp_symptoms),
                                    reply_markup=generate_symptoms_keyboard(usr.tmp_symptoms))

        return MAIN
    return button_callback