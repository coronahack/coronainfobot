from telegram import ParseMode
from telegram.ext import ConversationHandler, MessageHandler, Filters, CommandHandler, CallbackQueryHandler

import user
import scenario
import markup
import msg_gen
import symptoms_decision
import text
import config

import tree.symptoms
import tree.rights
import tree.otherhelp


WHAT_HAPPENS, \
SYMPTOMS, GROUP_RISK, POTENTIAL_CONTACTS,\
WHERE, DIAGNOSIS, OTHER_DIAGNOSIS, \
DETAILS, WHICH_HOSPITAL, HOSPITAL_NAME, \
AGREE_HOSPITAL, HOSPITAL_HELP, HEALTH_CARE, \
CONTACT, AGREEMENT = range(10, 25)


@user.wrap
def whappens(usr, upd, cnt):
    if not usr.focused: usr.focus_on_myself()
    upd.message.reply_text(**msg_gen.for_(usr).whappens)
    return WHAT_HAPPENS


@user.wrap
def diagnosis(u, update, context):
    update.message.reply_text('Вам поставили диагноз?',
                              reply_markup=markup.diagnosis)
    return DIAGNOSIS


@user.wrap
def potential_contacts(usr, upd, cnt):
    usr.save_group_risk(upd.message.text == 'Да')
    upd.message.reply_text('Были ли у вас контакты с потенциальными или подтвержденными носителями вируса?',
                           reply_markup=markup.yesno)
    return POTENTIAL_CONTACTS


@user.wrap
def group_risk(usr, upd, cnt):
    cnt.bot.send_message(chat_id=upd.effective_chat.id,
                         text=text.get('нужна помощь', 'группы риска', 'группы риска'),
                         reply_markup=markup.yesno)
    return GROUP_RISK

@user.wrap
def other_diagnosis(usr, upd, cnt):
    upd.message.reply_text('Введите диагноз текстом',
                           reply_markup=markup.undo)
    return OTHER_DIAGNOSIS

@user.wrap
def diagnosis_proceed(usr, upd, cnt):
    usr.save_diagnosis(upd.message.text)
    return end_tree(upd, cnt)


@user.wrap
def is_hospital(usr, upd, cnt):
    upd.message.reply_text(
        'Вы болеете дома или в больнице?' if usr.telling_about_myself else \
        'Человек о котором вы говорите болеет дома или в больнице?',
        reply_markup=markup.is_hospital
    )
    return WHERE


@user.wrap
def at_home(usr, upd, cnt):
    usr.save_place_home()
    return details(upd, cnt)


@user.wrap
def hospital(usr, upd, cnt):
    if not usr.have_hospitalized: usr.save_need_hospital_help(True)
    usr.save_hospitalized_start(True)
    upd.message.reply_text(
        'Вы знаете, в какую больницу вас везут?',
        reply_markup=markup.yesno_back
    )
    return WHICH_HOSPITAL


@user.wrap
def remove_hospitalized(usr, upd, cnt):
    usr.save_hospitalized_start(False)
    return whappens(upd, cnt)


@user.wrap
def in_hospital(usr, upd, cnt):
    upd.message.reply_text(
        'Введите текстом название больницы',
        reply_markup=markup.undo
    )
    return HOSPITAL_NAME


@user.wrap
def no_hospital(usr, upd, cnt):
    upd.message.reply_text(
        'Сообщите нам название больницы, как только узнаете.',
        reply_markup=markup.remove
    )
    return agree_hospital(upd, cnt)


@user.wrap
def get_hospital_name(usr, upd, cnt):
    usr.save_hospital(upd.message.text)
    return agree_hospital(upd, cnt)


@user.wrap
def agree_hospital(usr, upd, cnt):
    upd.message.reply_text(
        'Вы согласны с госпитализацией?',
        reply_markup=markup.yesno
    )
    return AGREE_HOSPITAL


@user.wrap
def details(usr, upd, cnt):
    upd.message.reply_text(
        'Хотите ли вы подробнее рассказать о своих симптомах?',
        reply_markup=markup.yesno
    )
    return DETAILS


@user.wrap
def share_contact(usr, upd, cnt):
    upd.message.reply_text(
        'Поделитесь своим контактом. Он пригодится для обратной связи с вами.',
        reply_markup=markup.contact
    )
    return CONTACT


@user.wrap
def get_contact(usr, upd, cnt):
    usr.save_phone_number(upd.message.contact.phone_number)
    return _end(upd, cnt)


@user.wrap
def no_contact(usr, upd, cnt):
    return _end(upd, cnt)


@user.wrap
def request_agreement(usr, update, context):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="Разрешаете ли вы «Новой газете» хранить и обрабатывать ваши данные в соответствии "
                                  "с нашей [Политикой конфиденциальности](https://novayagazeta.ru/privacy)? "
                                  "Мы собираем информацию в обезличенном виде для того, чтобы улучшать функции бота",
                             disable_web_page_preview=True,
                             parse_mode=ParseMode.MARKDOWN,
                             reply_markup=markup.data_agree)
    return AGREEMENT


@user.wrap
def handle_agreement(usr, upd, cnt):
    if usr.have_hospitalized_start and usr.hospitalized_start:
        usr.save_hospitalized(True)
        usr.save_hospitalized_start(False)
    usr.save_agreement(upd.message.text.startswith('Да'))
    return share_contact(upd, cnt) if usr.agreement else _end(upd, cnt)


@user.wrap
def handle_potential_contacts(usr, upd, cnt):
    usr.save_potential_contacts(upd.message.text == 'Да')
    if usr.have_hospitalized:
        return end_tree(upd, cnt)
    else:
        return symptoms_recommendation(upd, cnt)


@user.wrap
def symptoms_recommendation(usr, upd, cnt):
    reply_text = symptoms_decision.symptoms_decision(usr.tmp_symptoms, usr.group_risk, usr.potential_contacts)
    usr.save_symptoms_recommendation()
    upd.message.reply_text(reply_text, parse_mode=ParseMode.MARKDOWN)
    return end_tree(upd, cnt)


@user.wrap
def thanks(usr, upd, cnt):
    upd.message.reply_text("Спасибо за содействие!")
    return whappens(upd, cnt)


@user.wrap
def hospital_help(usr, update, context):
    usr.save_need_hospital_help(False)
    update.message.reply_text('Вы довольны оказываемой вам медицинской помощью?',
                              reply_markup=markup.yesno)
    return HOSPITAL_HELP

@user.wrap
def health_care_quality(usr, update, context):
    usr.save_health_care_quality(update.message.text=='Да')
    if usr.health_care_quality:
        return thanks(update, context)
    else:
        return bad_health_care_quality(update, context)

@user.wrap
def bad_health_care_quality(usr, update, context):
    update.message.reply_text(
        "Чем именно вы недовольны? Расскажите, я постараюсь помочь.",
        reply_markup=markup.end
    )
    return HEALTH_CARE


@user.wrap
def health_care_feedback(usr, update, context):
    update.message.forward(chat_id=config.rights_help)
    if usr.have_phone_number:
        context.bot.send_contact(chat_id=config.rights_help, phone_number=usr.phone_number,
                                 first_name=update.effective_user.first_name,
                                 last_name=update.effective_user.last_name)
    update.message.reply_text(
        "Спасибо что рассказали нам об этом! "
        "Вы можете продолжить писать сюда или закончить, нажав *Вернуться*.",
        reply_markup=markup.end,
        parse_mode=ParseMode.MARKDOWN
    )


@user.wrap
def just_testing(usr, upd, cnt):
    usr.new()
    return whappens(upd, cnt)


@user.wrap
def _end(usr, upd, cnt):
    if usr.have_need_hospital_help and usr.need_hospital_help:
        return hospital_help(upd, cnt)
    else:
        return thanks(upd, cnt)


@user.wrap
def end_tree(usr, upd, cnt):
    if not usr.have_agreement or not usr.agreement:
        return request_agreement(upd, cnt)
    elif not usr.have_contact:
        return share_contact(upd, cnt)
    else:
        return _end(upd, cnt)


@user.wrap
def diagnosis_hospital(u, update, context):
    context.bot.send_message(
        'Вам поставили диагноз?',
        reply_markup=markup.diagn_hospital,
    )
    return


@user.wrap
def how_long(u, update, context):
    update.bot.reply_text(
        'Как давно испытываете симптомы?\n(Напишите текстом)')
    return


handler = ConversationHandler(
    entry_points=[
        MessageHandler(Filters.regex('^(Нужна помощь)$'), whappens)
    ],

    states={

        WHAT_HAPPENS: [MessageHandler(Filters.regex('^Обнаружил'), tree.symptoms.callback),
                       MessageHandler(Filters.regex('^(Обновить симптомы|Рассказать о симптомах)$'),
                                      tree.symptoms.callback),
                       tree.rights.handler,
                       MessageHandler(Filters.regex('^Меня госпитализируют'), hospital),
                       MessageHandler(Filters.regex('^(Обновить статус госпитализации)$'), hospital),
                       MessageHandler(Filters.regex('^(Не устраивает медицинская помощь)$'),
                                      bad_health_care_quality),
                       tree.otherhelp.handler],

        tree.symptoms.MAIN: [
            CallbackQueryHandler(tree.symptoms.button(group_risk))
        ],

        GROUP_RISK: [
            MessageHandler(Filters.regex('^(Да|Нет)$'), potential_contacts),
        ],

        POTENTIAL_CONTACTS: [
            MessageHandler(Filters.regex('^(Да|Нет)$'), handle_potential_contacts)
        ],

        WHERE: [
            MessageHandler(Filters.regex('^(Дома)$'), at_home),
            MessageHandler(Filters.regex('^(В больнице)$'), in_hospital),
            MessageHandler(Filters.regex('^(Отмена|Вернуться)$'), remove_hospitalized)
        ],

        WHICH_HOSPITAL: [
            MessageHandler(Filters.regex('^(Да)$'), in_hospital),
            MessageHandler(Filters.regex('^(Нет)$'), no_hospital),
            MessageHandler(Filters.regex('^(Отмена|Вернуться)$'), remove_hospitalized),
            MessageHandler(Filters.text, get_hospital_name)
        ],

        HOSPITAL_NAME: [
            MessageHandler(Filters.regex('^(Отмена|Вернуться)$'), remove_hospitalized),
            MessageHandler(Filters.text, get_hospital_name)
        ],

        AGREE_HOSPITAL: [
            MessageHandler(Filters.regex('^(Да|Понятно)$'), diagnosis),
            MessageHandler(Filters.regex('^(Нет)$'), text.callback('нужна помощь', 'мои права нарушают',
                                                                   'Меня принудительно госпитализируют_Я против госпитализации. Что делать',
                                                                   parse_mode=ParseMode.MARKDOWN,
                                                                   state=AGREE_HOSPITAL))
        ],

        DIAGNOSIS: [MessageHandler(Filters.regex('^(Другой диагноз)$'), other_diagnosis),
                    MessageHandler(Filters.text, diagnosis_proceed)],

        OTHER_DIAGNOSIS: [
            MessageHandler(Filters.regex('^(Отмена)$'), diagnosis),
            MessageHandler(Filters.text, diagnosis_proceed)
        ],

        CONTACT: [
            MessageHandler(Filters.contact, get_contact),
            MessageHandler(Filters.regex('^(Не хочу)$'), _end)
        ],

        AGREEMENT: [
            MessageHandler(Filters.regex('^(Да|Нет)'), handle_agreement),
            MessageHandler(Filters.regex('^(Я просто тестирую бота)$'), just_testing)
        ],

        HOSPITAL_HELP: [
            MessageHandler(Filters.regex('^(Да|Нет)$'), health_care_quality)
        ],

        HEALTH_CARE: [
            MessageHandler(Filters.regex('^(Вернуться)$'), whappens),
            MessageHandler(Filters.all, health_care_feedback)
        ]

    },

    fallbacks=[MessageHandler(Filters.regex('^(Вернуться)$'), scenario.return_to_main),
               CommandHandler('cancel', scenario.return_to_main),
               CommandHandler('start', scenario.return_to_main)],

    map_to_parent={scenario.MAIN: scenario.MAIN,
                   scenario.END: scenario.MAIN}
)