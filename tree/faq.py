from telegram.ext import MessageHandler, Filters, ConversationHandler, CommandHandler

import scenario
import markup
import user
import text
import stickers

MAIN = 0


# context.bot.send_photo(chat_id=update.effective_chat.id,photo=
# "https://static- 0.rosminzdrav.ru/system/attachments/attaches/
# 000/049/317/big/WhatsApp_Image_2020-01-31_at_18.32.55.jpeg?1580484799")

@user.wrap
def what(usr, upd, cnt):
    upd.message.reply_text("Берегите себя и близких!",
                           reply_markup=markup.faq_questions)
    upd.effective_chat.send_sticker(sticker=stickers.what)
    return MAIN


@user.wrap
def what_again(usr, upd, cnt):
    upd.message.reply_text("Хотите ли вы уточнить что-то еще?",
                           reply_markup=markup.faq_questions)
    return MAIN


handler = ConversationHandler(
    entry_points=[MessageHandler(Filters.regex('^(Напомнить меры профилактики)$'), what)],

    states={
        MAIN: [MessageHandler(Filters.regex('^(Понятно)$'), what_again),
               MessageHandler(Filters.regex(r'^(Как не заразиться\?)$'),
                              text.callback('нужна помощь', 'Напомнить меры профилактики', 'Как не заразиться',
                                            state=MAIN, parse_mode="Markdown",
                                            sticker=stickers.wash_your_hands)),
               MessageHandler(Filters.regex(r'^(Как не заразить окружающих\?)$'),
                              text.callback('нужна помощь', 'Напомнить меры профилактики', 'Как не заразить окружающих',
                                            state=MAIN, parse_mode="Markdown",
                                            sticker=stickers.stay_home)),
               MessageHandler(Filters.regex(r'^(Когда стоит вызывать врача\?)$'),
                              text.callback('нужна помощь', 'Напомнить меры профилактики', 'Стоит ли вызывать врача',
                                            state=MAIN, parse_mode="MarkdownV2"))]
    },

    fallbacks=[MessageHandler(Filters.regex('^(Вернуться)$'), scenario.return_to_main),
               CommandHandler('start', scenario.return_to_main)],

    map_to_parent={scenario.MAIN: scenario.MAIN}
    )
