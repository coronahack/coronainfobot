from telegram.ext import CommandHandler, MessageHandler, Filters, ConversationHandler
from telegram import ReplyKeyboardMarkup, KeyboardButton, ParseMode

import time

import config
import user
import scenario
import text
import markup

VIOLATED, WHAT, WHAT_GUIDANCE, HELP, MEDICAL, HOSPITAL, WORK = range(7)


@user.wrap
def info(usr, upd, cnt):
    upd.message.reply_text(text.get('нужна помощь', 'Хочу получить юридическую консультацию'),
                           parse_mode='MarkdownV2')
    return what(upd, cnt)

@user.wrap
def info_guidance(usr, upd, cnt):
    upd.message.reply_text(text.get('нужна помощь', 'Хочу получить юридическую консультацию'),
                           parse_mode='MarkdownV2')
    return what_guidance(upd, cnt)



@user.wrap
def what(usr, upd, cnt):
    upd.message.reply_text("Какая у вас проблема?",
                           reply_markup=markup.types_of_offense)
    return WHAT

@user.wrap
def what_guidance(usr, upd, cnt):
    upd.message.reply_text("Что вы хотели бы узнать?",
                           reply_markup=markup.types_of_guidance_offense)
    return WHAT_GUIDANCE


def what_again(upd, cnt):
    upd.message.reply_text("Нужна ли вам еще юридическая помощь?",
                           reply_markup=markup.types_of_offense)
    return WHAT

def what_guidance_again(upd, cnt):
    upd.message.reply_text("Нужна ли вам еще юридическая помощь?",
                           reply_markup=markup.types_of_guidance_offense)
    return WHAT_GUIDANCE


@user.wrap
def medical(usr, upd, cnt):
    upd.message.reply_text("Как именно вам не оказывают медицинскую помощь?",
                           reply_markup=markup.rights_medical)
    return MEDICAL

@user.wrap
def hospital(usr, upd, cnt):
    upd.message.reply_text(text.get('нужна помощь', 'мои права нарушают',
                           'Меня принудительно госпитализируют_В каких случаях могут госпитализировать'),
                           parse_mode='Markdown')
    return hospital_what(upd, cnt)

@user.wrap
def hospital_what(usr, upd, cnt):
    upd.message.reply_text("Что именно происходит?",
                           reply_markup=markup.rights_hospital)
    return HOSPITAL


@user.wrap
def other(usr, upd, cnt):
    upd.message.reply_text("Опишите свою проблему ниже текстом, мы постараемся помочь",
                           reply_markup=markup.end)
    return HELP

@user.wrap
def work(usr, upd, cnt):
    upd.message.reply_text("Какая именно у вас проблема?",
                           reply_markup=markup.work_troubles)
    return WORK


@user.wrap
def help_forward(usr, upd, cnt):
    upd.message.forward(chat_id=config.rights_help)
    if usr.have_phone_number:
        cnt.bot.send_contact(chat_id=config.rights_help, phone_number=usr.phone_number,
                             first_name=upd.effective_user.first_name,
                             last_name=upd.effective_user.last_name)
    upd.message.reply_text('Спасибо за ваше обращение, оно передано правозащитникам. ' +
                           'Вы можете его дополнить ниже или нажать "Закончить"')
    return HELP


handler = ConversationHandler(
    entry_points=[MessageHandler(Filters.regex('^(Юридическая консультация)$'), info),
                  MessageHandler(Filters.regex('^(Мои права нарушают)$'), info),
                  MessageHandler(Filters.regex('^(Советы юристов)$'), info_guidance)],

    states={
        VIOLATED: [MessageHandler(Filters.regex('^(Да|Нет)$'), what)],

        WHAT: [MessageHandler(Filters.regex('^(Понятно)$'), what_again),
               MessageHandler(Filters.regex('^(Принудительно госпитализируют)$'), hospital),
               MessageHandler(Filters.regex('^(Проблемы с работой)$'), work),
               MessageHandler(Filters.regex('^(Задержали полицейские)$'),
                              text.callback('нужна помощь', 'мои права нарушают', 'Как вести себя при задержании',
                                            state=WHAT)),
               MessageHandler(Filters.regex('^(Проблемы в университете)$'),
                              text.callback('нужна помощь', 'мои права нарушают', 'Проблемы в университете',
                                            state=WHAT))],
            

        WHAT_GUIDANCE: [MessageHandler(Filters.regex('^(Понятно)$'), what_guidance_again),
                        MessageHandler(Filters.regex('^(Что будет, если нарушить карантин\?)$'),
                                       text.callback('нужна помощь', 'мои права нарушают', 
                                                     'Что будет, если нарушить карантин',
                                                     state=WHAT_GUIDANCE)),
                        MessageHandler(Filters.regex('^(Как вести себя при задержании\?)$'),
                                       text.callback('нужна помощь', 'мои права нарушают', 
                                                     'Как вести себя при задержании',
                                                     state=WHAT_GUIDANCE)),
                        MessageHandler(Filters.regex('^(Могут ли меня уволить\?)$'),
                                       text.callback('нужна помощь', 'мои права нарушают', 
                                                     'Могут ли меня уволить',
                                                     state=WHAT_GUIDANCE))],

        MEDICAL: [MessageHandler(Filters.regex('^(Понятно)$'), medical),
                  MessageHandler(Filters.regex('^(Вернуться)$'), what_again),
                  MessageHandler(Filters.regex('^(Не говорят результаты тестов)$'),
                                 text.callback('нужна помощь', 'мои права нарушают',
                                               'Не оказывают медицинскую помощь_Не говорят результаты тестов',
                                               state=MEDICAL, parse_mode=ParseMode.MARKDOWN)),
                  MessageHandler(Filters.regex('^(Поместили в общую палату с подозрением на коронавирус)$'),
                                 text.callback('нужна помощь', 'мои права нарушают',
                                               'Не оказывают медицинскую помощь_' +\
                                               'Поместили в общую палату с подозрением на коронавирус',
                                               state=MEDICAL))],

        HOSPITAL: [MessageHandler(Filters.regex('^(Понятно)$'), hospital),
                   MessageHandler(Filters.regex('^(Вернуться)$'), what_again),
                   MessageHandler(Filters.regex('^(Я против госпитализации. Что делать\?)$'),
                                  text.callback('нужна помощь', 'мои права нарушают',
                                                'Меня принудительно госпитализируют_' +\
                                                'Я против госпитализации. Что делать',
                                                state=HOSPITAL, parse_mode=ParseMode.MARKDOWN)),
                   MessageHandler(Filters.regex('^(Мне угрожают уголовной ответственностью. Это законно\?)$'),
                                  text.callback('нужна помощь', 'мои права нарушают',
                                                'Меня принудительно госпитализируют_' + \
                                                'Мне угрожают уголовной ответственностью за отказ в госпитализации.' + \
                                                ' Это возможно',
                                                state=HOSPITAL, parse_mode=ParseMode.MARKDOWN)),
                   ],

        WORK: [MessageHandler(Filters.regex('^(Понятно)$'), work),
               MessageHandler(Filters.regex('^(Вернуться)$'), what_again),
               MessageHandler(Filters.regex('^(Сократили на работе)$'),
                              text.callback('нужна помощь', 'мои права нарушают', 'Могут ли меня уволить',
                                            state=WORK, parse_mode=ParseMode.MARKDOWN_V2)),
               MessageHandler(Filters.regex('^(Работодатель подвергает риску)$'),
                              text.callback('нужна помощь', 'мои права нарушают', 'Работодатель подвергает риску',
                                            state=WORK, parse_mode=ParseMode.MARKDOWN_V2))],


        HELP: [MessageHandler(Filters.regex('^(Вернуться)$'), what_again),
               MessageHandler(Filters.text, help_forward)]
    },


    fallbacks=[MessageHandler(Filters.regex('^(Вернуться)$'), scenario.return_to_main),
               CommandHandler('start', scenario.return_to_main)],

    map_to_parent={scenario.MAIN: scenario.MAIN}
)
