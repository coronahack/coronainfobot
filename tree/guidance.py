from telegram.ext import CommandHandler, MessageHandler, Filters, ConversationHandler
from telegram import ReplyKeyboardMarkup, KeyboardButton, ParseMode

import user
import api
import scenario
import markup
import text

import tree.faq
import tree.rights


KIND_OF_HELP, ASK_LOCATION, VOLUNTEER = range(3)


@user.wrap
def help_callback(u, update, context):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="Что вам подсказать?",
                             reply_markup=markup.types_of_help)
    return KIND_OF_HELP


@user.wrap
def help_callback_again(u, update, context):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="Нужна ли вам еще помощь?",
                             reply_markup=markup.types_of_help)
    return KIND_OF_HELP


@user.wrap
def connect_volunteer(usr, upd, cnt):
    upd.message.reply_text("Как вам могут помочь волонтеры?",
                           reply_markup=markup.volunteer_help)
    return VOLUNTEER


handler = ConversationHandler(
    entry_points=[MessageHandler(Filters.regex('^(Полезно знать)$'), help_callback)],

    states={
        KIND_OF_HELP: [tree.faq.handler,
                       tree.rights.handler,
                       MessageHandler(Filters.regex('^(Понятно)$'), help_callback_again)
                      ],

        ASK_LOCATION: [MessageHandler(Filters.regex('^(Отмена)$'), connect_volunteer),
                       MessageHandler(Filters.all, scenario.wrong(ASK_LOCATION))],

        VOLUNTEER: [MessageHandler(Filters.regex('^(Понятно)$'), connect_volunteer),
                    MessageHandler(Filters.regex('^(Вернуться)$'), help_callback_again),
                    MessageHandler(Filters.regex('^(Мне нужны продукты питания)$'),
                                   text.callback('нужна помощь', 'Связать с волонтерами', 'Мне нужны продукты питания',
                                                 state=VOLUNTEER)),
                    MessageHandler(Filters.regex('^(Мне необходима психологическая помощь)$'),
                                   text.callback('нужна помощь', 'Связать с волонтерами', 'Мне необходима психологическая помощь',
                                                 state=VOLUNTEER))]
    },

    fallbacks=[MessageHandler(Filters.regex('^(Вернуться)$'), scenario.return_to_main),
               CommandHandler('start', scenario.return_to_main)],

    map_to_parent={scenario.MAIN: scenario.MAIN}
)
