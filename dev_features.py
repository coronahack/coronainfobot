from telegram.ext import CommandHandler, ConversationHandler, MessageHandler, Filters

from bot import bot, dp

import config
import user

def save_text(update: object, context):
    msg = update.message
    with open("res/{0}_{1}.txt".format(msg.from_user.username, msg.message_id), "w") as f:
        f.write(msg.text_markdown_v2_urled)
    update.message.reply_text("Текст сохранен!")
    return ConversationHandler.END


def save_highlight(update, context):
    update.message.reply_text("Пришлите пожалуйста текст в ответ")
    return "TEXT"

dp.add_handler(
    ConversationHandler(
        entry_points=[CommandHandler('save', save_highlight)],

        states={
            "TEXT": [MessageHandler(Filters.text, save_text)]
        },

        fallbacks=[]
    ))


def verbalize_chat(update, context):
    update.message.reply_text(str(update.effective_chat.id))

dp.add_handler(CommandHandler('verbalize', verbalize_chat))

import stickers
dp.add_handler(stickers.handler)


@user.wrap
def wipe(usr, update, context):
    usr.new()
    update.message.reply_text('Вся пользовательская информация удалена')

dp.add_handler(CommandHandler('wipe', wipe))