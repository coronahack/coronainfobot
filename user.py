from telegram import ParseMode
from telegram.ext import BaseFilter
from pymongo import errors
import pymongo
import hashlib

import json
import config
from datetime import datetime


def genhash(user_id):
    m = hashlib.sha224()
    m.update('corona_tgbot'.encode('ascii'))
    m.update(str(user_id).encode('ascii'))
    return m.hexdigest()


def timestamp():
    return datetime.now().strftime('%Y-%m-%d_%H:%M:%S')


class DB:
    def __init__(self, use_database=False,
                 url="", authenticate=False, username="", password="",
                 test_database=False, test_chat_id=0):
        self.use_database = use_database
        self.test_database = test_database
        self.test_chat_id = test_chat_id
        self._users = None
        if use_database:
            from pymongo import MongoClient
            mc = MongoClient(url)
            self.db = mc.bot_data
            if authenticate:
                self.db.authenticate(username, password)
            self.db.user_data.create_index([('user_id', pymongo.ASCENDING)], unique=True)
            self.db.states.create_index([('user_id', pymongo.ASCENDING)], unique=True)
            self.db.history.create_index([('user_hash', pymongo.ASCENDING)], unique=True)

        if test_database:
            from bot import bot
            self.bot = bot

    def save(self, d, action, save_to_db=False):
        d = d.copy()
        d['ts'] = timestamp()
        if self.use_database and save_to_db:
            self.db.user_data.update_one({'user_id': d['user_id']},
                                         {'$set': {'user_data': d}},
                                         upsert=True)

            self.db.states.update_one({'user_id': d['user_id']},
                                      {'$push': {'states': d}},
                                      upsert=True)

        if self.test_database:
            self.bot.send_message(chat_id=self.test_chat_id,
                                  text="*{1}*\n``` {0} ```".format(json.dumps(d, sort_keys=True, indent=2,
                                                                              ensure_ascii=False),
                                                                   action),
                                  parse_mode=ParseMode.MARKDOWN)

    def save_history(self, d, user_hash):
        if self.use_database:
            self.db.history.update_one({'user_hash': user_hash},
                                       {'$push': {'history': d}},
                                       upsert=True)

    def load_history(self, d_list, user_hash):
        if self.use_database:
            self.db.history.update_one({'user_id': user_hash},
                                       {'$addToSet': {'history': d_list}},
                                       upsert=True)

    @property
    def users(self):
        if self._users is None:
            u = dict()
            if self.use_database:
                for d in self.db.user_data.find():
                    u[d['user_id']] = d['user_data']
            self._users = u
        return self._users


db = DB(config.use_database,
        config.mongo_url, config.authenticate_mongo,
        config.mongo_username, config.mongo_password,
        config.test_database, config.errorlog)
# db = DB(use_database=True, database_name='test', test_database=False)
users = dict()
properties = json.load(open('user_properties.json'))


class User:

    MAX_OTHERS = 5

    def __init__(self, tg_user=None):
        if self._constructed: return
        if tg_user is not None:
            self.id = tg_user.id
            self.tg_first_name = tg_user.first_name if tg_user.first_name else ""
            self.tg_last_name = tg_user.last_name if tg_user.last_name else ""
            self.tg_username = tg_user.username
            users[self.id] = self
        else:
            self.id = None
            self.tg_first_name = None
            self.tg_last_name = None
            self.tg_username = None
        self.me_dict = dict()
        self.others = list()
        self.chosen = -2
        self.history = list()
        self.initialized = False
        self._constructed = True

    def __new__(cls, tg_user=None):
        if tg_user is not None:
            if tg_user.id in users.keys():
                return users[tg_user.id]
            elif tg_user.id in db.users.keys():
                u = cls.from_dict(db.users[tg_user.id])
                users[u.id] = u
                return u
        u = object.__new__(cls)
        u._constructed = False
        return u

    @staticmethod
    def from_dict(d):
        u = User()
        u.id = d['user_id']
        u.me_dict = d
        return u

    @property
    def can_save_data(self):
        if self.focused:
            return self.have_agreement and self.agreement
        else:
            return False

    def save_to_db(self, action, d=None):
        db.save(d if d is not None else self.cur,
                action,
                self.can_save_data)
        self.trace(action, "db")

    def trace(self, action, action_type=None):
        d = {'type': action_type,
             'action': action,
             'ts': timestamp()}
        #if self.can_save_data:
        #    if self.history is not None:
        #        self.history.append(d)
        #        db.load_history(self.history, self.id)
        #        self.history = None
        #    else:
        #        db.save_history(d, self.id)
        #else:
        #    if self.history is None: self.history = list()
        #    self.history.append(d)
        db.save_history(d, genhash(self.id))

    @property
    def telling_about_myself(self):
        return self.chosen == -1

    @property
    def telling_about_other(self):
        return self.chosen >= 0

    @property
    def told_about_myself(self):
        return 'checkpoint' in self.me_dict.keys()

    @property
    def told_about_other(self):
        return any(['checkpoint' in o.keys() for o in self.others])

    @property
    def cur(self):
        if self.telling_about_myself:
            return self.me_dict
        elif self.telling_about_other:
            return self.others[self.chosen]
        else:
            raise TypeError

    def new(self):
        self.me_dict = dict()
        self._init_dict(self.me_dict)
        self.me_dict['name'] = "{0} {1}".format(self.tg_first_name, self.tg_last_name).rstrip(" ").lstrip(" ")
        self.me_dict['username'] = self.tg_username
        self.me_dict['is_user'] = True
        self.save_to_db('new', self.me_dict)

    def start(self):
        if not self.initialized:
            self.new()
            self.initialized = True

    def new_other(self):
        if len(self.others) >= User.MAX_OTHERS: raise IndexError
        other_dict = dict()
        self._init_dict(other_dict)
        other_dict['is_user'] = False
        self.cpk[len(self.others)] = False
        self.others.append(other_dict)
        self.save_to_db('new_other', other_dict)

    @property
    def is_available_other(self):
        return len(self.others) <= User.MAX_OTHERS

    def _init_dict(self, d):
        d['user_id'] = self.id
        d['is_user'] = None
        for p in properties:
            d['have_'+p] = False

    @property
    def focused(self):
        return self.chosen >= -1

    def focus_on(self, i):
        self.chosen = i

    def focus_on_myself(self):
        self.focus_on(-1)

    def focus_on_new_other(self):
        self.focus_on(len(self.others) - 1)

    def cancel(self):
        pass

    def __getattr__(self, item):
        if item.startswith('save_'):
            item_name = item[5:]
            mode = 'save'
        elif item.startswith('have_'):
            item_name = item[5:]
            mode = 'have'
        else:
            item_name = item
            mode = 'get'

        if item_name not in properties:
            raise AttributeError('There is no "{0}" property!'.format(item_name))

        if mode == 'save':
            def save_data(d=None):
                try:
                    json.dumps(d)
                except TypeError:
                    data = d.to_dict()
                else:
                    data = d
                self.cur[item_name] = data
                self.cur['have_' + item_name] = True
                self.save_to_db(item_name)
            return save_data
        elif mode == 'have' or mode == 'get':
            return self.cur[item]

    @property
    def names_of_others(self):
        return [o['name'] for o in self.others]


def wrap(cb_with_user):
    def cb(update, context):
        usr = User(update.effective_user)
        usr.trace(update.effective_message.text if update.effective_message else None,
                  "update")
        result = cb_with_user(usr, update, context)
        usr.trace(cb_with_user.__name__, "callback")
        return result
    return cb


# -- Filters part --

class UserFilter(BaseFilter):
    def filter(self):
        raise NotImplemented


class FilterMeta(type):
    def __getattr__(cls, item):
        f = UserFilter()

        def func(msg):
            return getattr(User(msg.from_user), item)
        f.filter = func
        return f


class Filter(metaclass=FilterMeta):
    pass