import requests
import json

baseurl = 'https://api.covidarnost.ru/'

def datareq(suffix, d):
    return requests.post(baseurl+suffix, data={'data' : json.dumps(d)})

coords = lambda loc: {'coords': '{0},{1}'.format(loc['latitude'], loc['longitude'])}

def request_local_chat(loc):
    try:
        r = datareq('chat/getChat/', coords(loc))
    except:
        pass
    else:
        if r.ok: return r.json()
    return None

def create_chat(loc, url):
    d = coords(loc)
    d['url'] = url
    r = datareq('chat/createChat/', d)
    return r.ok
